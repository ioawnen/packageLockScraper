import requests
import sys
import json
import os

### This filthy hack of a script that rips through an input package-lock.json file and downloads all the dependencies it finds ###
### ioawnen ###


def get_file_json(file_path):
    ### Get the contents of a json file at the input path ###
    data = {}
    try:
        with open(file_path) as f:
            data = json.load(f)
    except Exception as exc:
        print('ERROR - Error reading input file. Does the file exist? Is is valid JSON?')
        print(exc)
        sys.exit(1)  # Can't do shit without this. Fail if errored
    return data


def ensure_dir(path):
    ### Check if dir exists, make it if not ###
    directory = os.path.dirname(path)
    if not os.path.exists(directory):
        os.makedirs(directory)


def build_dep_data(name, data):
    ### deps are names attributes not very useful when we're build a list of dicts ###
    return {
        'name': name,
        'data': data
    }


def get_dependencies(obj):
    ### Recurse through all dependencies and their children, build a list of everything we find ###
    dependencies = []

    for attr, value in obj.items():
        dependencies.append(build_dep_data(attr, value))
        if 'dependencies' in value:
            sub_deps = value['dependencies']
            if sub_deps and len(sub_deps) > 0:
                dependencies = dependencies + get_dependencies(sub_deps)

    return dependencies


def clean_dependencies_list(obj):
    # TODO: WE SHOULD REPORT ANYTHING WE REMOVE USING THIS!
    return [x for x in obj if 'resolved' in x['data']]


def download_file(url, path, filename=None):
    local_filename = filename or url.split('/')[-1]
    r = requests.get(url, stream=True)
    with open(path+local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    return local_filename


def main():

    # INPUT FILE CHECK
    if not len(sys.argv) > 1:
        print('ERROR - NO FILE PATH INPUT. Did you specify the file to use? eg: python nodeDownload.py package-lock.json')
        sys.exit(1)  # Can't do shit without this. Fail if not found

    # OPTIONAL DESTINATION CHECK
    destination_path = 'files'
    if len(sys.argv) > 2:
        destination_path = sys.argv[2]
    else:
        print(
            'WARN - No destination defined, using default ({0})'.format(destination_path))
    if not destination_path.endswith('/'):
        destination_path += '/'

    # PREPARE DESTINATION
    ensure_dir(destination_path)

    # GET INPUT FILE CONTENTS
    file_contents = get_file_json(sys.argv[1])

    # GET LIST OF DEPS
    dependencies = sorted(clean_dependencies_list(get_dependencies(
        file_contents['dependencies'])), key=lambda k: k['name'])

    # FETCH DEPS
    for dep in dependencies:
        print(
            'FETCH:\t{0}\n\tVersion:\t{1}\n\tSource: \t{2}\n'.format(
                dep['name'], dep['data']['version'], dep['data']['resolved']))

        download_file(dep['data']['resolved'], destination_path)


if __name__ == '__main__':
    print('packageLockScraper - Scrapes package-lock.json files and downloads everything it finds.\nAuthor: ioawnen\n')
    main()
    print('\nComplete!')
    sys.exit(0)
