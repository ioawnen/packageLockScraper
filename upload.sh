REGISTRY_URL=localhost:8081/repository/npm_test/
REGISTRY_URL_PREFIX=http://
FILES_DIR=./test

USERNAME=test
PASSWORD=test

echo "Setting registry...."
npm config set registry $REGISTRY_URL_PREFIX$REGISTRY_URL
echo "Registry set. Registry: $(npm config get registry)"

echo -e "\nLogging into registry and setting auth token...."
LOGIN_JSON=$(\
    curl -s -H "Accept: application/json" \
    -H "Content-Type:application/json" \
    -X PUT \
    --data '{"name": "'$USERNAME'", "password": "'$PASSWORD'"}' \
    $REGISTRY_URL_PREFIX$REGISTRY_URL-/user/org.couchdb.user:$USERNAME \
)
# Strip out the auth token. I'm using python for this because I can
AUTH_TOKEN=$(python -c "import json; print(json.loads('$LOGIN_JSON')['token'])")
npm config set //$REGISTRY_URL:_authToken $AUTH_TOKEN
echo "Token set. Token: $AUTH_TOKEN"

echo -e "\nPublishing files...."
for file in $FILES_DIR/*
do
    npm publish "$file" || exit 1
done

echo -e "\n\nComplete!"
exit 0
